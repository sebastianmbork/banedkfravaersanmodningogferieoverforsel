sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"dk2bmfravaersanmodingogferieoverfoersel/util/Controller",
	"sap/ui/core/format/DateFormat",
	"sap/ui/core/format/NumberFormat",
], function (BaseController, MessageBox, Utilities, History, Controller, DateFormat, NumberFormat) {
	"use strict";

	return BaseController.extend("dk2bmfravaersanmodingogferieoverfoersel.controller.AbsenceOverview", {
		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("LandingPage").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) {
			this.createFilterPeriodeDialog();
			this.createGroupDialog();

			this.getUsersAbsenceSet().then(function () {
				this.createFilterDialog();
			}.bind(this), function (error) {
				this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
			}.bind(this));

			// create filter
			this.aFilters = [];
			this.aSorters = [];

			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.subscribe("changeCalculationDate", this.updateAbsenceListBasedOnCalculationDate, this);
		},

		updateAbsenceListBasedOnCalculationDate: function (sChanel, sEvent, oData) {
			var calculationDate = oData.calculationDate;

			var aFilters = [];

			if (calculationDate) {
				aFilters.push(new sap.ui.model.Filter("CalculationDate", sap.ui.model.FilterOperator.EQ, calculationDate));
			}

			// update client side model and
			this.getUsersAbsenceSet(aFilters).then(function () {
				this.createFilterDialog();
			}.bind(this), function (error) {
				this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
			}.bind(this));

		},

		getUsersAbsenceSet: function (filter) {
			var sorter = [];
			sorter.push(new sap.ui.model.Sorter("StartDate", false));

			return new Promise(function (resolve, reject) {
				this.getView().getModel().read("/AbsenceSet", {
					filters: filter,
					sorters: sorter,
					success: function (data) {
						var results = data.results;

						var viewModel = this.getView().getModel("ViewModel");

						// map used to check which absenceType has been added
						var absenceTypeIdUniqueMap = {};

						// only add unique absence to usersAbsenceSet - used in filter dialog
						results.forEach(function (result) {
							result.SearchString = result.AbsenceDesc.toLowerCase();

							var added = absenceTypeIdUniqueMap[result.AbsenceTypeId];

							if (!added) {
								absenceTypeIdUniqueMap[result.AbsenceTypeId] = result;
								viewModel.getData().usersAbsenceSet.push(result);
							}
						});

						// used in absence table 
						viewModel.getData().absenceSet = results;
						viewModel.getData().absenceCount = results.length;
						viewModel.refresh();
						resolve();
					}.bind(this),
					error: function (error) {
						reject(error);

					}.bind(this)
				});
			}.bind(this));
		},

		createFilterPeriodeDialog: function () {
			var oView = this.getView();
			var absenceOverviewFilterPeriodeDialog = oView.byId("absenceOverviewFilterPeriodeDialog");
			// create dialog lazily
			if (!absenceOverviewFilterPeriodeDialog) {
				// create dialog via fragment factory
				absenceOverviewFilterPeriodeDialog = sap.ui.xmlfragment(oView.getId(),
					"dk2bmfravaersanmodingogferieoverfoersel.view.fragments.AbsenceOverview.FilterPeriodeDialog",
					this);
				oView.addDependent(absenceOverviewFilterPeriodeDialog);
				absenceOverviewFilterPeriodeDialog.setBusyIndicatorDelay(0);
			}
		},

		createFilterDialog: function () {
			var oView = this.getView();
			var absenceOverviewFilterDialog = oView.byId("absenceOverviewFilterDialog");
			// create dialog lazily
			if (!absenceOverviewFilterDialog) {
				// create dialog via fragment factory
				absenceOverviewFilterDialog = sap.ui.xmlfragment(oView.getId(),
					"dk2bmfravaersanmodingogferieoverfoersel.view.fragments.AbsenceOverview.FilterDialog",
					this);
			}
			absenceOverviewFilterDialog.setModel(this.getView().getModel("ViewModel"));
			oView.addDependent(absenceOverviewFilterDialog);
			absenceOverviewFilterDialog.setBusyIndicatorDelay(0);
		},

		createGroupDialog: function () {
			var oView = this.getView();
			var absenceOverviewSortDialog = oView.byId("absenceOverviewGroupDialog");
			// create dialog lazily
			if (!absenceOverviewSortDialog) {
				// create dialog via fragment factory
				absenceOverviewSortDialog = sap.ui.xmlfragment(oView.getId(),
					"dk2bmfravaersanmodingogferieoverfoersel.view.fragments.AbsenceOverview.GroupDialog",
					this);
				oView.addDependent(absenceOverviewSortDialog);
				absenceOverviewSortDialog.setBusyIndicatorDelay(0);
			}
		},

		openFilterDialog: function () {
			var oView = this.getView();
			var absenceOverviewFilterDialog = oView.byId("absenceOverviewFilterDialog");
			absenceOverviewFilterDialog.open();
		},

		openFilterPeriodeDialog: function () {
			var oView = this.getView();
			var absenceOverviewFilterPeriodeDialog = oView.byId("absenceOverviewFilterPeriodeDialog");
			absenceOverviewFilterPeriodeDialog.open();
		},

		openGroupDialog: function () {
			var oView = this.getView();
			var absenceOverviewSortDialog = oView.byId("absenceOverviewGroupDialog");
			absenceOverviewSortDialog.open();
		},

		closeSortDialog: function () {
			var oView = this.getView();
			var absenceOverviewSortDialog = oView.byId("absenceOverviewSortDialog");
			absenceOverviewSortDialog.close();

			this.updateActiveButtons();
		},

		closeFilterPeriodeDialog: function () {
			var oView = this.getView();
			var absenceOverviewFilterPeriodeDialog = oView.byId("absenceOverviewFilterPeriodeDialog");
			absenceOverviewFilterPeriodeDialog.close();

			this.updateActiveButtons();
		},

		confirmClientSideFilter: function (oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("absenceTable");

			var oBinding = oTable.getBinding("items");
			var mParams = oEvent.getParameters();

			this.removeExistingClientSideFilters().then(function () {
				// apply filters
				for (var i = 0, l = mParams.filterItems.length; i < l; i++) {
					var oItem = mParams.filterItems[i];
					var aSplit = oItem.getKey().split("___");
					var sPath = aSplit[0];
					var vOperator = aSplit[1];
					var vValue1 = aSplit[2];
					var vValue2 = aSplit[3];

					// check if boolean
					if (vValue1 === "true" || vValue1 === "false") {
						vValue1 = vValue1 === "true" ? true : false;
					}

					var oFilter = new sap.ui.model.Filter(sPath, vOperator, vValue1, vValue2);
					this.aFilters.push(oFilter);
				}

				oBinding.filter(this.aFilters);

				this.updateActiveButtons();
			}.bind(this));
		},

		removeExistingClientSideFilters: function () {
			return new Promise(function (resolve) {
				var tempArray = [];

				this.aFilters.forEach(function (filter) {
					if (filter.sPath === "StartDate" || filter.sPath === "EndDate") {
						tempArray.push(filter);
					}
				}.bind(this));

				this.aFilters = [];
				this.aFilters = tempArray;

				resolve();
			}.bind(this));
		},

		confirmClientSideGroup: function (oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("absenceTable");

			var oBinding = oTable.getBinding("items");
			var mParams = oEvent.getParameters();

			// clear sort array
			this.aSorters = [];

			// apply grouping
			if (mParams.groupItem) {
				var sPath = mParams.groupItem.getKey();
				var type = sPath;
				var bDescending = mParams.groupDescending;
				var vGroup = function (oContext) {
					var name = oContext.getProperty(type);
					return {
						key: name,
						text: name
					};
				};
				this.aSorters.push(new sap.ui.model.Sorter(sPath, bDescending, vGroup));
			}

			// single sort need to be after group sort
			//this.aSortersSearch.push(this.singleSortInbox);

			oBinding.sort(this.aSorters);

			this.updateActiveButtons();

		},

		updateActiveButtons: function () {
			// update date button
			var dateButtonpressed = false;
			this.aFilters.forEach(function (filter) {
				if (filter.sPath === "StartDate" || filter.sPath === "EndDate") {
					dateButtonpressed = true;
				}
			}.bind(this));
			this.getView().byId("periodButton").setPressed(dateButtonpressed);

			// update filter button
			if (this.aFilters.length > 0) {
				this.aFilters.forEach(function (filter) {
					if (filter.sPath !== "StartDate" && filter.sPath !== "EndDate") {
						this.getView().byId("filterButton").setPressed(true);
					} else {
						this.getView().byId("filterButton").setPressed(false);
					}
				}.bind(this));
			} else {
				this.getView().byId("filterButton").setPressed(false);
			}

			// update group sort button
			var groupButtonpressed = false;
			if (this.aSorters) {
				this.aSorters.forEach(function (filter) {
					if (filter.vGroup) {
						groupButtonpressed = true;
					}
				}.bind(this));
			}
			this.getView().byId("groupButton").setPressed(groupButtonpressed);

		},

		confirmClientSideSortDept: function (oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("absenceTable");

			var oBinding = oTable.getBinding("items");
			var mParams = oEvent.getParameters();
			var sPath = mParams.sortItem.getKey();
			var bDescending = mParams.sortDescending;

			// apply sorter
			this.removeExistingSingleSort().then(function () {
				this.aSorters.push(new sap.ui.model.Sorter(sPath, bDescending));
				oBinding.sort(this.aSorters);

				this.updateActiveButtons();
			}.bind(this));

		},

		removeExistingSingleSort: function () {
			var temp = [];
			return new Promise(function (resolve) {
				this.aSorters.forEach(function (filter) {
					if (filter.vGroup) {
						temp.push(filter);
					}
				}.bind(this));
				this.aSorters = [];
				this.aSorters = temp;
				resolve();
			}.bind(this));
		},

		setPeriodeFilter: function (oEvent) {
			var startDate = this.getView().byId("fromDate").getDateValue();
			var endDate = this.getView().byId("toDate").getDateValue();

			if (startDate) {
				var filterDate = new Date(startDate.toString());
				var offSetFromDate = startDate.getTimezoneOffset() * -1;
				filterDate.setDate(startDate.getDate() - 1);
				filterDate.setMinutes(startDate.getMinutes() + offSetFromDate);
			}
			if (endDate) {
				var offSetToDate = endDate.getTimezoneOffset() * -1;
				endDate.setDate(endDate.getDate() + 1);
				endDate.setMinutes(endDate.getMinutes() + offSetToDate);
			}

			this.filterOnDates(filterDate, endDate);
		},

		filterOnDates: function (startDate, endDate) {
			var oView = this.getView();
			var oTable = oView.byId("absenceTable");

			var oBinding = oTable.getBinding("items");

			this.removeExistingDatesInFilter().then(function () {
				if (startDate) {
					var fromDateFilter = new sap.ui.model.Filter("StartDate", sap.ui.model.FilterOperator.GT, startDate);
					this.aFilters.push(fromDateFilter);
				}

				if (endDate) {
					var toDateFilter = new sap.ui.model.Filter("EndDate", sap.ui.model.FilterOperator.LE, endDate);
					this.aFilters.push(toDateFilter);
				}
				oBinding.filter(this.aFilters);

				this.closeFilterPeriodeDialog();

				this.updateActiveButtons();
			}.bind(this));
		},

		onClearDateFilter: function (oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("absenceTable");
			var oBinding = oTable.getBinding("items");

			this.removeExistingDatesInFilter().then(function () {
				// clear values
				this.getView().byId("fromDate").setValue("");
				this.getView().byId("toDate").setValue("");

				// update filter
				oBinding.filter(this.aFilters);

				this.updateActiveButtons();
			}.bind(this));
		},

		removeExistingDatesInFilter: function () {
			// remove frontend filter to ensure only 1 startdate and 1 enddate filter to ensure liveChange is not affected
			return new Promise(function (resolve) {
				var tempArray = [];

				this.aFilters.forEach(function (filter) {
					if (filter.sPath !== "StartDate" && filter.sPath !== "EndDate") {
						tempArray.push(filter);
					}
				}.bind(this));

				this.aFilters = [];
				this.aFilters = tempArray;

				resolve();
			}.bind(this));
		},

		removeExistingSearchFilterInFilter: function () {
			// remove frontend filter to ensure only 1 filter
			return new Promise(function (resolve) {
				var tempArray = [];

				this.aFilters.forEach(function (filter) {
					if (filter.sPath !== "SearchString") {
						tempArray.push(filter);
					}
				}.bind(this));

				this.aFilters = [];
				this.aFilters = tempArray;

				resolve();
			}.bind(this));
		},

		liveChange: function (oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("absenceTable");
			var oBinding = oTable.getBinding("items");
			var sQuery = oEvent.getParameter("newValue").toLowerCase();

			this.removeExistingSearchFilterInFilter().then(function () {
				if (sQuery) {
					var oFilter = new sap.ui.model.Filter("SearchString", "Contains", sQuery);
					this.aFilters.push(oFilter);
				}

				oBinding.filter(this.aFilters);
			}.bind(this));
		},
		/*
				formatDates: function (startDate, endDate, absenceType) {
					return this.getOwnerComponent().formatStartEndDate(startDate, endDate, "", this.getView().getModel("ViewModel"));
				},*/
		/*
				formatDates: function (startDate, endDate, isAllDay, deductionDays, deductionHours) {
					var oDateFormat = DateFormat.getInstance({
						pattern: "d. MMM y",
						UTC: true
					});

					if (startDate !== undefined && endDate !== undefined) {
						if ((!isAllDay || endDate === undefined) || (startDate.getDate() === endDate.getDate() && startDate.getMonth() === endDate.getMonth() &&
								startDate
								.getFullYear() === endDate
								.getFullYear()) && deductionDays !== "0.00" && deductionHours !== "0.00") {

							var oDateFormatWithFromHour = DateFormat.getInstance({
								pattern: "d. MMM y",
								UTC: true
							});

							var oDateFormatWithHourWithoutDate = DateFormat.getInstance({
								pattern: "HH:mm",
								UTC: true
							});

							var klokken = this.getView().getModel("i18n").getResourceBundle().getText(
								"HolidayRequest-kl");

							if (oDateFormatWithHourWithoutDate.format(startDate) === "00:00" && oDateFormatWithHourWithoutDate.format(endDate) === "00:00") {
								return oDateFormatWithFromHour.format(startDate);
							} else {
								return oDateFormatWithFromHour.format(startDate) + "\n" + klokken + " " + oDateFormatWithHourWithoutDate.format(startDate) + "-" +
									oDateFormatWithHourWithoutDate.format(endDate);
							}

						} else {
							return oDateFormat.format(startDate) + " - " + oDateFormat.format(endDate);
						}
					}
				},*/

		formatDates: function (absenceType, startDate, endDate, isAllDay, deductionDays, deductionHours) {
			if (startDate !== undefined && endDate !== undefined) {
				var oDateFormat = DateFormat.getInstance({
					pattern: "d. MMM y",
					UTC: true
				});

				var absenceTypeObj = this.getView().getModel("ViewModel").getData().absenceTypeSet[absenceType];

				// same day - including time
				if ((!isAllDay || endDate === undefined) || (startDate.getDate() === endDate.getDate() && startDate.getMonth() === endDate.getMonth() &&
						startDate
						.getFullYear() === endDate
						.getFullYear()) && deductionDays !== "0.00" && deductionHours !== "0.00") {

					// do not show time if deductionDays === "1.00" or if UnitISO == "DAY" and is not all day
					if (deductionDays === "1.00" || (absenceTypeObj && absenceTypeObj.UnitISO === "DAY" && !isAllDay)) {
						return oDateFormat.format(startDate);
					}
					
					
					var oDateFormatWithFromHour = DateFormat.getInstance({
						pattern: "d. MMM y",
						UTC: true
					});

					var oDateFormatWithHourWithoutDate = DateFormat.getInstance({
						pattern: "HH:mm",
						UTC: true
					});

					var klokken = this.getView().getModel("i18n").getResourceBundle().getText(
						"HolidayRequest-kl");
						
					var startTime = oDateFormatWithHourWithoutDate.format(startDate);
					var endTime = oDateFormatWithHourWithoutDate.format(endDate);
					
					// show time it is defined
					if(startTime !== "00:00" && endTime !== "00:00"){
						return oDateFormatWithFromHour.format(startDate) + "\n" + klokken + " " + startTime + "-" +
						endTime;
					}else{
						return oDateFormat.format(startDate);
					}
				} else if (deductionDays === "0.00" && deductionHours === "0.00") {
					// only show start date
					return oDateFormat.format(startDate);
				} else {
					// show start and end date
					return oDateFormat.format(startDate) + " - " + oDateFormat.format(endDate);
				}
			}
		},

		showAbsenceDaysOrLeaveHours: function (absenceType, calendarDays, calendarHours) {
			var absenceTypeObj = this.getView().getModel("ViewModel").getData().absenceTypeSet[absenceType];

				if (absenceTypeObj && absenceTypeObj.UnitISO === "DAY") {
					return calendarDays;
				}
				
				if(!absenceTypeObj){
					console.log("Fejl: kan ikke finde absence type for: " + absenceType);
				}
			

			return calendarHours;
		},

		showDayOrHour: function (absenceType, absenceDays, asbsenceHours) {
			
			var absenceTypeObj = this.getView().getModel("ViewModel").getData().absenceTypeSet[absenceType];
			
			if (absenceTypeObj && absenceTypeObj.UnitISO === "DAY") {
				// check if deductionDays equals 1.00
				var day = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-dag");
				var days = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-dage");
				return absenceDays === "1.00" ? day : days;
			} else {
				// check if deductionHours equals to 1.00
				var hour = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-time");
				var hours = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-timer");
				return asbsenceHours === "1.00" ? hour : hours;
			}

		},
		/*
				absenceSetUpdateFinished: function (oEvent) {
					var total = oEvent.getParameter("total");
					var viewModel = this.getView().getModel("ViewModel");
					if (viewModel.getData().absenceCount === "") {
						viewModel.getData().absenceCount = total;
						viewModel.refresh();
					}
				}*/

	});
}, /* bExport= */ true);