sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"dk2bmfravaersanmodingogferieoverfoersel/util/Controller",
	"sap/ui/core/format/DateFormat",
	"sap/ui/core/format/NumberFormat",
], function (BaseController, MessageBox, Utilities, History, Controller, DateFormat, NumberFormat) {
	"use strict";

	return BaseController.extend("dk2bmfravaersanmodingogferieoverfoersel.controller.HolidayRequest", {
		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("LandingPage").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) {
			// set periodStartDate and periodEndDate by todays date
			var date = new Date();
			date.setSeconds("00");
			this.setVacationPeriodSpanSet(new Date());
		},

		changeCalculationDate: function (oEvent) {
			var datevalue = oEvent.getParameter("value").split("-");
			var date = new Date(datevalue[2] + "-" + datevalue[1] + "-" + datevalue[0]);
			this.updateQuotasBasedOnCalculationDate(date);
			this.setVacationPeriodSpanSet(date);
			
			// publish event to update absence table
			var eventBus = sap.ui.getCore().getEventBus();
			var data = {
				calculationDate: date
			};
			eventBus.publish("changeCalculationDate", data);
			
			
		},

		updateQuotasBasedOnCalculationDate: function (calculationDate) {
			var oTable = this.getView().byId("quotasTable");
			var oBinding = oTable.getBinding("items");

			var aFilters = [];

			if (calculationDate) {
				aFilters.push(new sap.ui.model.Filter("CalculationDate", sap.ui.model.FilterOperator.EQ, calculationDate));
			}

			oBinding.filter(aFilters);
		},

		setVacationPeriodSpanSet: function (calculationDate) {
			var aFilters = [];

			if (calculationDate) {
				aFilters.push(new sap.ui.model.Filter("CalculationDate", sap.ui.model.FilterOperator.EQ, calculationDate));
			}

			this.getView().getModel().read("/VacationPeriodSpanSet", {
				filters: aFilters,
				success: function (data) {
					var periodStartDate = data.results[0].PeriodStart;
					var periodEndDate = data.results[0].PeriodEnd;
					var viewModel = this.getView().getModel("ViewModel");
					viewModel.getData().periodStart = periodStartDate.getDate() + "." + (periodStartDate.getMonth() + 1) + "." + periodStartDate.getFullYear();
					viewModel.getData().periodEnd = periodEndDate.getDate() + "." + (periodEndDate.getMonth() + 1) + "." + periodEndDate.getFullYear();
					viewModel.refresh();
				}.bind(this),
				error: function (error) {
					this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
				}.bind(this)
			});
		}

	});
}, /* bExport= */ true);