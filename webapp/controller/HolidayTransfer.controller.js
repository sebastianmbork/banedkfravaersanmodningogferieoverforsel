sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"dk2bmfravaersanmodingogferieoverfoersel/util/Controller"
], function (BaseController, MessageBox, Utilities, History, Controller) {
	"use strict";

	return BaseController.extend("dk2bmfravaersanmodingogferieoverfoersel.controller.HolidayTransfer", {
		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("LandingPage").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

		},

		handleRouteMatched: function (oEvent) {
			
		},

		onAfterRendering: function () {
		},
		


		deleteHolidayTransfer: function (oEvent) {
			var path = oEvent.getParameter('listItem').getBindingContextPath();
			var number = path.split("requestSet/");
			var statusId = "";
			var approver = "";
			var text;

			if (statusId === "APPROVED") {
				approver = this.getView().getModel("UserModel").getData().managerFullName;
				/*
				var statusDesc = obj.StatusDesc.toLowerCase();
				text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-erDuSikkerPåAtDuVilSletteAnmodningGodkendelse", [
					statusDesc,
					approver
				]);
				*/
				text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-erDuSikkerPåAtDuVilSletteGodkendtAnmodning", [
					approver
				]);
				
				
			} else { // status equals "Send"
				text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-erDuSikkerPåAtDuVilSletteAnmodning");
			}
			this.getOwnerComponent().showDeleteRequestMessageBox(text, approver, statusId).then(function (message) {
				/*if (message.length > 0) {
					this.deleteHolidayRequestWithUpdate(message, obj.RequestId);
				} else {
					this.deleteHolidayRequest(obj.RequestId);
				}*/
				//this.deleteHolidayRequest(obj.RequestId);
				var holidayTransferModel = this.getView().getModel("holidayTransferModel");
				holidayTransferModel.getData().requestSet.splice(number[1], 1);
				holidayTransferModel.refresh();
			}.bind(this), function () {
				// reject
			}.bind(this));
		},
		
		openMessageHistory: function (oEvent) {
			// test data
			//var obj = oEvent.getParameter('listItem').getObject();
			var obj = oEvent.getSource().getParent().getBindingContext("holidayTransferModel").getObject();
			var msgToApprover = obj.message;
			var msgOtherAgreements = obj.otherAgreements;
			var msgApprover = "";
			var userString = this.getOwnerComponent().getUserName() + " (03.12.2019):";
			var approverString = this.getOwnerComponent().getApproverName() + " (" + "obj.ApproverMessageDate" + "):";

			var content = this.getOwnerComponent().createMessageHistoricHolidayTransferContent(msgToApprover, msgOtherAgreements, msgApprover, userString, approverString);
			this.getOwnerComponent().showHelpTextDialog(oEvent.getSource(), "Left", content);
		},
		
		showHolidayTransferHelpText: function(oEvent) {
			var text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayTransfer-anmodningHelpText");
			var content =  new sap.m.Text({
						text: text
					});
			this.getOwnerComponent().showHelpTextDialog(oEvent.getSource(), "Right", content);
		},

	});
}, /* bExport= */ true);