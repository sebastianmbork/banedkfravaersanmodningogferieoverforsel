sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"dk2bmfravaersanmodingogferieoverfoersel/util/Controller",
	"sap/ui/core/format/DateFormat",
	"sap/ui/core/format/NumberFormat",
], function (BaseController, MessageBox, Utilities, History, Controller, DateFormat, NumberFormat) {
	"use strict";

	return BaseController.extend("dk2bmfravaersanmodingogferieoverfoersel.controller.LeaveRequests", {
		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("LandingPage").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) {
			// get absence type set as a map to be used by a formatter in quotasTable
			this.getAbsenceTypeSet();
		},

		getAbsenceTypeSet: function (calculationDate) {
			this.getView().getModel().read("/AbsenceTypeSet", {
				success: function (data) {
					var results = data.results;
					var viewModel = this.getView().getModel("ViewModel");

					results.forEach(function (result) {
						viewModel.getData().absenceTypeSet[result.AbsenceTypeId] = result;
					});
					viewModel.refresh();
				}.bind(this),
				error: function (error) {
					this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
				}.bind(this)
			});
		},

		onDeleteHolidayRequest: function (oEvent) {
			var obj = oEvent.getSource().getParent().getBindingContext().getObject();
			var statusId = obj.StatusId;
			var approver = "";
			var text;

			if (statusId === "APPROVED" || statusId === "POSTED") {
				approver = this.getView().getModel("UserModel").getData().managerFullName;
				text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-erDuSikkerPåAtDuVilSletteGodkendtAnmodning", [
					approver
				]);
				
			} else { // status equals "Send"
				text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-erDuSikkerPåAtDuVilSletteAnmodning");
			}
			this.getOwnerComponent().showDeleteRequestMessageBox(text, approver, statusId).then(function (message) {
				/*if (message.length > 0) {
					this.deleteHolidayRequestWithUpdate(message, obj.RequestId);
				} else {
					this.deleteHolidayRequest(obj.RequestId);
				}*/
				this.deleteHolidayRequest(obj.RequestId);
			}.bind(this), function () {
				// reject
			}.bind(this));
		},
/*
		deleteHolidayRequestWithUpdate: function (msg, requestId) {
			var data = {
				CurrNotice: "sletning - besked:" + msg
			};

			var leaveRequestId = "guid'" + requestId + "'";
			this.getView().getModel().update("/LeaveRequestSet(" + leaveRequestId + ")", data, {
				success: function (oData) {
					// refresh holidayRequestTable
					this.getView().byId("holidayRequestTable").getModel().refresh();
				}.bind(this),
				error: function (error) {
					this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
				}.bind(this)
			});
		},*/

		deleteHolidayRequest: function (requestId) {
			var leaveRequestId = "guid'" + requestId + "'";
			var parameters = {
				success: function () {
					// refresh holidayRequestTable
					this.getView().byId("holidayRequestTable").getModel().refresh();
				}.bind(this),
				error: function (error) {
					this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
				}.bind(this)
			};
			this.getView().getModel().remove("/LeaveRequestSet(" + leaveRequestId + ")", parameters);
		},

		showHelpText: function (oEvent) {
			var text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-fraværsanmodningHelpText");
			var content = new sap.m.Text({
				text: text
			});
			this.getOwnerComponent().showHelpTextDialog(oEvent.getSource(), "Right", content);
		},
		
		showStatusHelpText: function (oEvent) {
			var text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-fraværsanmodningStatusHelpText");
			var content = new sap.m.Text({
				text: text
			});
			this.getOwnerComponent().showHelpTextDialog(oEvent.getSource(), "Right", content);
		},
		
		showAnsoegtHelpText: function (oEvent) {
			var text = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-fraværsanmodningAnsoegtHelpTex");
			var content = new sap.m.Text({
				text: text
			});
			this.getOwnerComponent().showHelpTextDialog(oEvent.getSource(), "Right", content);
		},

		calculatePlanned: function (entitled, unused) {
			return entitled - unused;
		},

		translateStatusId: function (statusId) {
			if (statusId === "REJECTED") {
				return "Error";
			} else if (statusId === "APPROVED" || statusId === "POSTED") {
				return "Success";
			} else {
				return "Warning";
			}
		},

		formatLeaveRequestDates: function (isAllDay, absenceType, startDate, endDate, deductionDays, deductionHours) {
			if (startDate !== undefined && endDate !== undefined) {
				var oDateFormat = DateFormat.getInstance({
					pattern: "d. MMM y",
					UTC: true
				});
				
				var absenceTypeObj = this.getView().getModel("ViewModel").getData().absenceTypeSet[absenceType];
				
				// same day - including time
				if (endDate === undefined || (startDate.getDate() === endDate.getDate() && startDate.getMonth() === endDate.getMonth() &&
						startDate
						.getFullYear() === endDate
						.getFullYear()) && deductionDays !== "0.00" && deductionHours !== "0.00") {
					
					// do not show time if deductionDays === "1.00" or if UnitISO == "DAY" and is not all day
					if(deductionDays === "1.00" || (absenceTypeObj && absenceTypeObj.UnitISO === "DAY" && !isAllDay)) {
						return oDateFormat.format(startDate);
					}
					
					var oDateFormatWithFromHour = DateFormat.getInstance({
						pattern: "d. MMM y",
						UTC: true
					});

					var oDateFormatWithHourWithoutDate = DateFormat.getInstance({
						pattern: "HH:mm",
						UTC: true
					});

					var klokken = this.getView().getModel("i18n").getResourceBundle().getText(
						"HolidayRequest-kl");
					return oDateFormatWithFromHour.format(startDate) + "\n" + klokken + " " + oDateFormatWithHourWithoutDate.format(startDate) + "-" +
						oDateFormatWithHourWithoutDate.format(endDate);
				} else if (deductionDays === "0.00" && deductionHours === "0.00") {
					// only show start date
					return oDateFormat.format(startDate);
				} else {
					// show start and end date
					return oDateFormat.format(startDate) + " - " + oDateFormat.format(endDate);
				}
			}
		},

		showDaysOrHours: function (absenceType, absenceDays, leaveHours, deductionDays, deductionHours) {
			var absenceTypeObj = this.getView().getModel("ViewModel").getData().absenceTypeSet[absenceType];
			 
			if (absenceTypeObj && absenceTypeObj.UnitISO === "DAY") {
				return absenceDays + " | " + deductionDays;
			} else {
				return leaveHours + " | " + deductionHours;
			}
			
			if(!absenceTypeObj) {
				console.log("Fejl:" + "absenceType" + "findes ikke");
			}
		},

		showDayOrHour: function (absenceType, deductionDays, deductionHours) {
			var absenceTypeObj = this.getView().getModel("ViewModel").getData().absenceTypeSet[absenceType];

			if (absenceTypeObj && absenceTypeObj.UnitISO === "DAY") {
				// check if deductionDays equals 1.00
				var day = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-dag");
				var days = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-dage");
				return deductionDays === "1.00" ? day : days;
			} else {
				// check if deductionHours equals to 1.00
				var hour = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-time");
				var hours = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-timer");
				return deductionHours === "1.00" ? hour : hours;
			}
			
			if(!absenceTypeObj) {
				console.log("Fejl:" + "absenceType" + "findes ikke");
			}

		},

		openMessageHistory: function (oEvent) {
			var obj = oEvent.getSource().getParent().getBindingContext().getObject();
			var pastNotice = obj.PastNotice.split("/n");
			var pastNoticeDate = pastNotice[1].split(" ")[0];
			var msg = pastNotice[2];
			var msgApprover = "obj.ApproverMessage";
			var userString = this.getOwnerComponent().getUserName() + " (" + pastNoticeDate + "):";
			var approverString = this.getOwnerComponent().getApproverName() + " (" + "obj.ApproverMessageDate" + "):";

			var content = this.getOwnerComponent().createMessageHistoricContent(msg, msgApprover, userString, approverString);
			this.getOwnerComponent().showHelpTextDialog(oEvent.getSource(), "Left", content);
		},

		leaveRequestSetUpdated: function (oEvent) {
			var total = oEvent.getParameter("total");
			var viewModel = this.getView().getModel("ViewModel");
			viewModel.getData().leaveRequestsCount = total;
			viewModel.refresh();
		},
		
		showStatusDesc: function(statusId) {
			if(statusId === "SENT") {
				return "Afsendt";
			}else if(statusId === "REJECTED") {
				return "Afivst";
			}else if(statusId === "POSTED") {
				return "Godkendt og registreret";
			}else{
				return "Godkendt";
			}
		}

	});
}, /* bExport= */ true);