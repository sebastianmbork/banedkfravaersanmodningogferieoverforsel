sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History",
	"dk2bmfravaersanmodingogferieoverfoersel/util/Controller",
	"sap/ui/core/format/NumberFormat",
	"sap/ui/core/format/DateFormat"
], function (BaseController, MessageBox, Utilities, History, Controller, NumberFormat, DateFormat) {
	"use strict";

	return BaseController.extend("dk2bmfravaersanmodingogferieoverfoersel.controller.LandingPage", {
		onInit: function () {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("LandingPage").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) { 
			var today = new Date();
			var day = today.getDate();
			var month = today.getMonth() + 1;
			var year = today.getFullYear();

			// create json model
			var viewModel = new sap.ui.model.json.JSONModel({
				fravær: true,
				today: today,
				todayStr: day + "-" + month + "-" + year,
				periodStart: "",
				periodEnd: "",
				leaveRequestsCount: "",
				absenceCount: "",
				absenceTypeSet: {},
				usersAbsenceSet: [], // used as filter options in absence overview
				absenceSet: []
			});
			this.getView().setModel(viewModel, "ViewModel");

			// client model - only used for sprint 3
			var holidayTransferModel = new sap.ui.model.json.JSONModel({
				quotaSet: [{
					quota: "Ferie med løn",
					periode: "01.09.2019-31.04.2020",
					unused: 4,
					spent: 21,
					request: 4,
					value: "Dage"
				}],
				requestSet: [{
					type: "Udbetal særlige ferie dage",
					aplicationDate: "02.12.2019",
					days: 4,
					status: "Afsendt",
					value: "Dage",
					statusId: "",
					otherAgreements: "",
					message: ""
				}]
			});
			this.getView().setModel(holidayTransferModel, "holidayTransferModel");
		},

		onAfterRendering: function () {
			this.createHolidayRequestDialog();
			this.createHolidayTransferDialog();
		},

		selectIconTab: function (oEvent) {
			var selectedKey = oEvent.getParameter("selectedKey");
			var viewModel = this.getView().getModel("ViewModel");
			if (selectedKey === "Fravær") {
				viewModel.getData().fravær = true;
			} else {
				viewModel.getData().fravær = false;
			}
			viewModel.refresh();
		},

		createHolidayRequestDialog: function () {
			var oView = this.getView();
			var holidayRequestDialog = oView.byId("holidayRequestDialog");
			// create dialog lazily
			if (!holidayRequestDialog) {
				// create dialog via fragment factory
				holidayRequestDialog = sap.ui.xmlfragment(oView.getId(),
					"dk2bmfravaersanmodingogferieoverfoersel.view.fragments.HolidayRequestDialog",
					this);
				oView.addDependent(holidayRequestDialog);
				holidayRequestDialog.setBusyIndicatorDelay(0);
			}
		},

		onOpenHolidayRequestDialog: function (oEvent) {
			var oView = this.getView();
			var holidayRequestDialog = oView.byId("holidayRequestDialog");

			var oModel = new sap.ui.model.json.JSONModel({
				"ErrorMessageMissingValues": "",
				"absenceTypeHours": false
			});
			holidayRequestDialog.setModel(oModel, "holidayRequestModel");
			holidayRequestDialog.setModel(this.getView().getModel("UserModel"), "UserModel");

			// Create new entry
			var newEntry = this.getView().getModel().createEntry("/LeaveRequestSet");

			// Bind new item to popover
			holidayRequestDialog.setBindingContext(newEntry);

			// bind is all day to true as default
			var bindingContextPath = holidayRequestDialog.getBindingContext().getPath();
			this.getView().getModel().setProperty(bindingContextPath + "/IsAllDay", true);

			this.getView().byId("timePickerStart").setValue("");
			this.getView().byId("timePickerEnd").setValue("");

			holidayRequestDialog.open();
		},

		onCloseHolidayRequestDialog: function () {
			if (this.getView().getModel().hasPendingChanges()) {
				this.getView().getModel().resetChanges();
			}

			var holidayRequestDialog = this.getView().byId("holidayRequestDialog");
			holidayRequestDialog.close();
		},

		onHolidayRequestSelectDayType: function (oEvent) {
			var holidayRequestDialog = this.getView().byId("holidayRequestDialog");
			var holidayRequestModel = holidayRequestDialog.getModel("holidayRequestModel");
			var context = holidayRequestDialog.getBindingContext();

			// reset values
			this.getView().getModel().setProperty("LeaveStart", undefined, context);
			this.getView().getModel().setProperty("LeaveEnd", undefined, context);
			this.getView().getModel().setProperty("LeaveHours", undefined, context);
			this.getView().getModel().setProperty("LeaveDays", undefined, context);
			this.getView().byId("timePickerStart").setValue("");
			this.getView().byId("timePickerEnd").setValue("");
			holidayRequestModel.getData().ErrorMessageMissingValues = "";

			this.updateUserModelAbsenceTypeHours();
		},

		updateUserModelAbsenceTypeHours: function () {
			var holidayRequestDialog = this.getView().byId("holidayRequestDialog");
			var holidayRequestModel = holidayRequestDialog.getModel("holidayRequestModel");
			if (!holidayRequestModel.getData().isLeavReqTimePeriodUsed) {
				var unitISO = this.getView().byId("selectAbesenceType").getSelectedItem().getBindingContext().getObject().UnitISO;

				if (unitISO === "HUR") {
					holidayRequestModel.getData().absenceTypeHours = true;
				} else {
					holidayRequestModel.getData().absenceTypeHours = false;
				}
				holidayRequestModel.refresh();
			}
		},

		changeAbsenceType: function () {
			this.updateUserModelAbsenceTypeHours();
		},

		convertDateToUTC: function (date) {
			return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
		},

		onCreateHolidayRequest: function () {
			var context = this.getView().byId("holidayRequestDialog").getBindingContext();
			var isLeavReqTimePeriodUsed = this.getView().getModel("UserModel").getData().isLeavReqTimePeriodUsed;

			var isAllDay = this.getView().getModel().getProperty("IsAllDay", context);

			// set absenceType
			var absenceType = this.getView().byId("selectAbesenceType").getSelectedKey();
			this.getView().getModel().setProperty("AbsenceType", absenceType, context);

			// isLeavReqTimePeriodUsed and is all day used
			if (isAllDay) {
				this.leavReqTimePeriodUsedAndAllDay(context);
			}

			// isLeavReqTimePeriodUsed and is all day used
			if (isLeavReqTimePeriodUsed && !isAllDay) {
				this.leavReqTimePeriodUsedAndNotAllDay(context);
			}

			// not isLeavReqTimePeriodUsed and is all day used
			if (!isLeavReqTimePeriodUsed && !isAllDay) {
				this.notLeavReqTimePeriodUsedAndNotAllDay(context);
			}
		},

		notLeavReqTimePeriodUsedAndNotAllDay: function (context) {
			var leaveStart = this.getView().getModel().getProperty("LeaveStart", context);
			var leaveHours = this.getView().getModel().getProperty("LeaveHours", context);
			var leaveDays = this.getView().getModel().getProperty("LeaveDays", context);
			var holidayRequestDialog = this.getView().byId("holidayRequestDialog");
			var absenceTypeHours = holidayRequestDialog.getModel("holidayRequestModel").getData().absenceTypeHours;
			var validation = true;

			if (leaveStart !== undefined) {
				// update leave start to adjust utc
				var timeOffsetLeaveStart = Math.abs(leaveStart.getTimezoneOffset());
				leaveStart.setMinutes(leaveStart.getMinutes() + timeOffsetLeaveStart);

				// set leavened to equal leaveStart
				this.getView().getModel().setProperty("LeaveEnd", leaveStart, context);
			}

			// validation
			if (leaveStart === undefined || (absenceTypeHours && leaveHours === undefined) || (!absenceTypeHours && leaveDays === undefined)) {
				validation = false;
			}

			this.postHolidayRequest(validation);
		},

		leavReqTimePeriodUsedAndAllDay: function (context) {
			var leaveStart = this.getView().getModel().getProperty("LeaveStart", context);
			var leaveEnd = this.getView().getModel().getProperty("LeaveEnd", context);
			var validation = true;

			// update leave start to adjust utc
			if (leaveStart !== undefined) {
				var timeOffsetLeaveStart = Math.abs(leaveStart.getTimezoneOffset());
				leaveStart.setMinutes(leaveStart.getMinutes() + timeOffsetLeaveStart);
			}

			// update leave start to adjust utc
			if (leaveEnd !== undefined) {
				var timeOffsetLeaveEnd = Math.abs(leaveEnd.getTimezoneOffset());
				leaveEnd.setMinutes(leaveEnd.getMinutes() + timeOffsetLeaveEnd);
			}

			// validation
			if (leaveStart === undefined || leaveEnd === undefined) {
				validation = false;
			}

			this.postHolidayRequest(validation);
		},

		leavReqTimePeriodUsedAndNotAllDay: function (context) {
			var leaveStart = this.getView().getModel().getProperty("LeaveStart", context);
			var timePickerStart = this.getView().byId("timePickerStart").getValue();
			var timePickerEnd = this.getView().byId("timePickerEnd").getValue();
			var validation = true;

			if (leaveStart && timePickerStart !== "" && timePickerEnd !== "") {
				// set leaveend
				var timePickerEndArray = timePickerEnd.split(".");
				var newLeaveEnd = new Date(leaveStart.getFullYear(), leaveStart.getMonth(), leaveStart.getDate(), timePickerEndArray[0],
					timePickerEndArray[1]);
				var timeOffsetNewLeaveEnd = Math.abs(newLeaveEnd.getTimezoneOffset());
				newLeaveEnd.setMinutes(newLeaveEnd.getMinutes() + timeOffsetNewLeaveEnd);
				this.getView().getModel().setProperty("LeaveEnd", newLeaveEnd, context);

				// update leave start
				var timePickerStartArray = timePickerStart.split(".");
				leaveStart.setHours(timePickerStartArray[0]);
				leaveStart.setMinutes(timePickerStartArray[1]);
				leaveStart.setMinutes(leaveStart.getMinutes() + timeOffsetNewLeaveEnd);
			} else {
				validation = false;
			}

			this.postHolidayRequest(validation);
		},

		postHolidayRequest: function (validation) {
			var holidayTransferDialog = this.getView().byId("holidayRequestDialog");
			var holidayRequestModel = holidayTransferDialog.getModel("holidayRequestModel");
			if (!validation) {
				holidayRequestModel.getData().ErrorMessageMissingValues = this.getView().getModel("i18n").getResourceBundle().getText(
					"HolidayRequest-errorMessage");
				holidayRequestModel.refresh();
				return;
			} else {
				holidayRequestModel.getData().ErrorMessageMissingValues = "";
				holidayRequestModel.refresh();
			}

			holidayTransferDialog.setBusy(true);

			this.getView().getModel().submitChanges({
				success: function (oResponse) {

					// Reset view busy
					holidayTransferDialog.setBusy(false);

					// Close the popup
					holidayTransferDialog.close();

					var error = oResponse.__batchResponses[0].response;

					var errorCodes400sReges = new RegExp("^([4][0-9][0-9])$");
					var errorCodes500sReges = new RegExp("^([5][0-9][0-9])$");
					if (errorCodes400sReges.test(error.statusCode) || errorCodes500sReges.test(error.statusCode)) {
						if (this.getView().getModel().hasPendingChanges()) {
							this.getView().getModel().resetChanges();
						}
						// Show error to user
						this.getOwnerComponent().errorCallBackShowInPopUpSubmitChanges(error);
					} else {
						var title = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-succesfuldt");
						var msg = this.getView().getModel("i18n").getResourceBundle().getText("HolidayRequest-fraværsanmodingoprettet");
						// Succes Message Box
						this.getOwnerComponent().showSuccesMessageBox(title, msg);
					}
				}.bind(this),
				error: function (error) {
					// clear pending changes
					if (this.getView().getModel().hasPendingChanges()) {
						this.getView().getModel().resetChanges();
					}

					// Reset view busy
					holidayTransferDialog.setBusy(false);
					// Close the popup
					holidayTransferDialog.close();
				}.bind(this)
			});
		},

		createHolidayTransferDialog: function () {
			// create json model
			var holidayTransferDialogModel = new sap.ui.model.json.JSONModel({
				requestType: "",
				quote: "",
				days: "",
				otherAgreements: "",
				message: ""

			});
			this.getView().setModel(holidayTransferDialogModel, "holidayTransferDialogModel");

			var oView = this.getView();
			var holidayTransferDialog = oView.byId("holidayTransferDialog");
			// create dialog lazily
			if (!holidayTransferDialog) {
				// create dialog via fragment factory
				holidayTransferDialog = sap.ui.xmlfragment(oView.getId(),
					"dk2bmfravaersanmodingogferieoverfoersel.view.fragments.HolidayTransferDialog",
					this);
				oView.addDependent(holidayTransferDialog);
				holidayTransferDialog.setBusyIndicatorDelay(0);
				holidayTransferDialog.setModel(holidayTransferDialogModel);
			}
		},

		onCreateHolidayTransferRequest: function (oEvent) {
			var obj = oEvent.getSource().getModel("holidayTransferDialogModel").getData();
			var holidayTransferModel = this.getView().getModel("holidayTransferModel");
			holidayTransferModel.getData().requestSet.push({
				type: obj.requestType,
				aplicationDate: "03.12.2019",
				days: obj.days,
				status: "Afsendt",
				value: "Dage",
				statusId: "",
				otherAgreements: obj.otherAgreements,
				message: obj.message
			});
			holidayTransferModel.refresh();

			// clear model
			var model = oEvent.getSource().getModel("holidayTransferDialogModel");
			model.getData().requestType = "";
			model.getData().quote = "";
			model.getData().days = "";
			model.getData().otherAgreements = "";
			model.getData().message = "";
			model.refresh();

			var holidayTransferDialog = this.getView().byId("holidayTransferDialog");
			holidayTransferDialog.close();
		},

		onOpenHolidayTransferDialog: function (oEvent) {
			var oView = this.getView();
			var holidayTransferDialog = oView.byId("holidayTransferDialog");

			// Create new entry
			var newEntry = this.getView().getModel().createEntry("/SupervisorAgreementSet");

			// Bind new item to popover
			holidayTransferDialog.setBindingContext(newEntry);

			holidayTransferDialog.open();
		},

		onCloseHolidayTransferDialog: function () {
			if (this.getView().getModel().hasPendingChanges()) {
				this.getView().getModel().resetChanges();
			}

			var holidayTransferDialog = this.getView().byId("holidayTransferDialog");
			holidayTransferDialog.close();
		},

		showErrorMessage: function (message) {
			return message.length > 0;
		},

		showAbsenceType: function (absenceType) {
			return (absenceType !== undefined) ? absenceType : "Ferie med løn";
		},

	});
}, /* bExport= */ true);