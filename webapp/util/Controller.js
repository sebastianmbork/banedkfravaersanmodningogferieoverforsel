sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
], function(Controller, UIComponent) {
	"use strict";
	
	return Controller.extend("dk2bmfravaersanmodingogferieoverfoersel.util.Controller", {
		/**
		 * get the event bus of the applciation component
		 * @returns {Object} the event bus
		 */
		getEventBus: function() {
			return sap.ui.getCore().getEventBus();
		},

		/**
		 * get the UIComponent router
		 * @param{Object} this either a view or controller
		 * @returns {Object} the event bus
		 */
		getRouter: function() {
			return UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function() {
			if (sap.ui.getCore().getComponent(window.componentId).getModel("i18n")) {
				return sap.ui.getCore().getComponent(window.componentId).getModel("i18n").getResourceBundle();
			} else {
				return null;
			}
		},

		getI18nText: function(key) {
			var rb = this.getResourceBundle();

			if (rb) {
				return rb.getText(key);
			} else {
				return null;
			}
		},
		
		test: function() {
		 alert("hej");	
		},
		
		errorCallBackShowInPopUp: function(oError) {

			if (oError) {
				if (oError.responseText) {
					var errorCode = JSON.parse(oError.responseText).error.code;
					var errorMessage = JSON.parse(oError.responseText).error.message.value;

					if (sap.hybrid) {
						//Log application error
						sap.Logger.error(errorCode + " " + errorMessage);
					}

					sap.m.MessageBox.error(this.getI18nText("Controller-Error-Prefix") + ": " + errorCode + " - " + errorMessage);
				} else {
					if (sap.hybrid) {
						//Log application error
						sap.Logger.error(oError.message);
					}

					sap.m.MessageBox.error(this.getI18nText("Controller-Error-Prefix") + ": " + oError.message);
				}
			}
		},

		errorBatchCallBackShowInPopUp: function(oError) {

			if (oError) {
				if (oError.body) {
					var errorCode = JSON.parse(oError.body).error.code;
					var errorMessage = JSON.parse(oError.body).error.message.value;

					sap.m.MessageBox.error(this.getI18nText("Controller-Error-Prefix") + ": " + errorCode + " - " + errorMessage);
				} else {
					sap.m.MessageBox.error(this.getI18nText("Controller-Error-Prefix") + ": " + oError.message);
				}
			}
		},



	});
});