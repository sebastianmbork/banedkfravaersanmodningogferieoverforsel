sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"dk2bmfravaersanmodingogferieoverfoersel/model/models",
	"sap/ui/core/format/DateFormat",
	//"./model/errorHandling",
	"sap/m/Button",
	"sap/m/Dialog"
], function (UIComponent, Device, models, DateFormat, Button, Dialog) {
	"use strict";

	var navigationWithContext = {

	};

	return UIComponent.extend("dk2bmfravaersanmodingogferieoverfoersel.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			// set the FLP model
			this.setModel(models.createFLPModel(), "FLP");

			// set the dataSource model
			this.setModel(new sap.ui.model.json.JSONModel({}), "dataSource");

			// set application model
			var oApplicationModel = new sap.ui.model.json.JSONModel({});
			this.setModel(oApplicationModel, "applicationModel");

			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// delegate error handling
			//errorHandling.register(this);

			// create the views based on the url/hash
			this.getRouter().initialize();

			var user = new sap.ui.model.json.JSONModel();
			this.setModel(user, "UserModel");

			// set user details
			this.setUserDetails().then(function () {
				this.checkIfLeaveReqAllowed();
			}.bind(this));

			// variable to ensure only one session warning is showed
			this.sessionGoneWarningShowing = false;

			// check session expired every 3 minutes (180000)
			setInterval(function () {
				if (!this.sessionGoneWarningShowing) {
					this.checkForSession();
				}
			}.bind(this), 180000);
		},

		checkForSession: function () {
			var parameters = {
				success: function () {
					// do nothing
					console.log("session ok");
				},
				error: function (error) {
					this.sessionGoneWarningShowing = true;

					var msg = this.getModel("i18n").getResourceBundle().getText(
						"Warning-dinSessionErUdloebetGenindlaesSidenIgen");
					var title = this.getModel("i18n").getResourceBundle().getText(
						"Warning-advarlsel");
					this.showWarningDialogWithPromise(title, msg).then(function () {
						window.location.reload();
					});
				}.bind(this)
			};

			this.getModel().read("/LogonUserSet", parameters);
		},

		showWarningDialogWithPromise: function (title, msg) {
			return new Promise(function (resolve, reject) {
				var dialog = new Dialog({
					title: title,
					type: 'Message',
					state: 'Warning',
					content: new sap.m.Text({
						text: msg,
						width: "100%",
						textAlign: "Center"
					}),
					beginButton: new Button({
						text: 'OK',
						press: function () {
							dialog.close();
							resolve();
						}
					}),
					afterClose: function () {
						dialog.destroy();
					}
				});

				dialog.open();
			}.bind(this));
		},

		createContent: function () {
			var app = new sap.m.App({
				id: "App"
			});
			var appType = "App";
			var appBackgroundColor = "#FFFFFF";
			if (appType === "App" && appBackgroundColor) {
				app.setBackgroundColor(appBackgroundColor);
			}

			return app;
		},

		getNavigationPropertyForNavigationWithContext: function (sEntityNameSet, targetPageName) {
			var entityNavigations = navigationWithContext[sEntityNameSet];
			return entityNavigations == null ? null : entityNavigations[targetPageName];
		},

		checkIfLeaveReqAllowed: function () {
			if (!this.getModel("UserModel").getData().isLeaveReqAllowed) {
				var msg = this.getModel("i18n").getResourceBundle().getText(
					"HolidayRequest-dinBrugerHarIkkeTilladelseTilAtOpretteFraværsanmodninger");
				var dialog = new sap.m.Dialog({
					title: 'Information',
					type: 'Message',
					content: new sap.m.Text({
						text: msg
					}),
					endButton: new sap.m.Button({
						text: 'Ok',
						press: function () {
							dialog.close();
						}
					}),
					afterClose: function () {
						dialog.destroy();
					}
				});
				dialog.open();
			}
		},

		setUserDetails: function () {
			return new Promise(function (resolve, reject) {
				var parameters = {
					success: function (oData) {
						if (oData.results.length > 0) {
							this.getModel("UserModel").setData({
								fullName: oData.results[0].FirstName + " " + oData.results[0].LastName,
								employeeId: oData.results[0].EmployeeId,
								userId: oData.results[0].UserId,
								isLeavReqTimePeriodUsed: oData.results[0].IsLeavReqTimePeriodUsed,
								isLeaveReqAllowed: oData.results[0].IsLeaveReqAllowed,
								managerFullName: oData.results[0].ManagerFullName
							});
						}
						this.getModel("UserModel").refresh();
						resolve();
					}.bind(this),

					error: function (error) {

					}.bind(this)
				};
				this.getModel().read("/LogonUserSet", parameters);
			}.bind(this));
		},

		getUserName: function () {
			return this.getModel("UserModel").getData().fullName;
		},

		getUserId: function () {
			return this.getModel("UserModel").getData().userId;
		},

		getApproverName: function () {
			return this.getModel("UserModel").getData().managerFullName;
		},

		showHelpTextDialog: function (element, placement, content) {
			var oPopover = new sap.m.ResponsivePopover({
				showHeader: false,
				contentWidth: "320px",
				placement: placement,
				content: content,
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						oPopover.close();
					}
				})
			});

			oPopover.addStyleClass("sapUiPopupWithPadding");
			oPopover.openBy(element);
		},

		showDeleteRequestMessageBox: function (message, approver, statusId) {
			return new Promise(function (resolve, reject) {
				var title = this.getModel("i18n").getResourceBundle().getText("HolidayRequest-sletFraværsanmodning");
				var labelText = this.getModel("i18n").getResourceBundle().getText("HolidayRequest-beskedTil", [
					approver
				]);

				var items = [
					new sap.m.Text({
						text: message
					})
				];

				// only add textArea if approver is defined - statusId === "Approved"
				/*
				if (approver !== "") {
					items.push(new sap.m.Label({
							text: labelText,
							design: "Bold"
						}).addStyleClass('sapUiSmallMarginTop'),
						new sap.m.TextArea({
							value: '{/message}'
						}).addStyleClass('twobmTextArea'));
				}*/

				var box = new sap.m.VBox({
					items: items
				});

				box.setModel(new sap.ui.model.json.JSONModel({
					message: ''
				}));
				sap.m.MessageBox.show(
					box, {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: title,
						actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.CANCEL],
						onClose: function (oAction) {
							if (oAction === sap.m.MessageBox.Action.YES) {
								// brug kun hvis der skal en besked tilbage til lederen
								//var message = box.getModel().getProperty('/message'); 
								//resolve(message);
								resolve();
							}
						},
						styleClass: "", // default
						initialFocus: null, // default
						textDirection: sap.ui.core.TextDirection.Inherit // default
					}
				);
			}.bind(this));
		},

		showSuccesMessageBox: function (title, msg) {
			sap.m.MessageBox.show(
				msg, {
					icon: sap.m.MessageBox.Icon.SUCCESS,
					title: title,
					actions: [sap.m.MessageBox.Action.OK],
					onClose: function (oAction) {}
				}
			);
		},

		createMessageHistoricHolidayTransferContent: function (msgToApprover, msgOtherAgreements, msgApprover, userString, approverString) {
			var items = [];

			if (msgOtherAgreements !== "") {
				items.push(new sap.m.Label({
					text: "Øvrige aftale:",
					design: "Bold"
				}));
				items.push(new sap.m.Text({
					text: userString
				}), new sap.m.Text({
					text: msgOtherAgreements
				}));
			}
			
			if (msgToApprover !== "") {
				items.push(new sap.m.Label({
					text: "Besked til leder:",
					design: "Bold"
				}).addStyleClass('sapUiMediumMarginTop'));
				items.push(new sap.m.Text({
					text: userString
				}), new sap.m.Text({
					text: msgToApprover
				}));
			}

			if (msgApprover !== undefined && msgApprover !== "") {
				items.push(new sap.m.Label({
					text: "Besked fra leder:",
					design: "Bold"
				}).addStyleClass('sapUiMediumMarginTop'));
				items.push(new sap.m.Text({
						text: approverString
					}).addStyleClass('sapUiMediumMarginTop'),
					new sap.m.Text({
						text: msgApprover
					}));
			}

			var content = new sap.m.VBox({
				items: items
			});

			return content;
		},

		createMessageHistoricContent: function (msg, msgApprover, userString, approverString) {
			var items = [new sap.m.Label({
				text: "Beskeder:",
				design: "Bold"
			})];

			if (msg !== "") {
				items.push(new sap.m.Text({
					text: userString
				}), new sap.m.Text({
					text: msg
				}));
			}

			if (msgApprover !== undefined && msgApprover !== "") {
				items.push(new sap.m.Text({
						text: approverString
					}).addStyleClass('sapUiMediumMarginTop'),
					new sap.m.Text({
						text: msgApprover
					}));
			}

			var content = new sap.m.VBox({
				items: items
			});

			return content;
		},

		errorCallBackShowInPopUpSubmitChanges: function (oError) {
			if (oError) {
				var errorText = this.getModel("i18n").getResourceBundle().getText("Controller-Error-Prefix");
				if (oError.body) {
					var errorCode = JSON.parse(oError.body).error.code;
					var errorMessage = JSON.parse(oError.body).error.message.value;

					sap.m.MessageBox.error(errorText + ": " + errorMessage);
				} else {
					var errorParsed = JSON.parse(oError.responseText);

					sap.m.MessageBox.error(errorText + ": " + errorParsed.error.message.value);
				}
			}
		},

		formatStartEndDate: function (startDate, endDate, absenceType, viewModel) {
			if (startDate !== undefined && endDate !== undefined) {

				if (endDate === undefined || (startDate.getDate() === endDate.getDate() && startDate.getMonth() === endDate.getMonth() &&
						startDate
						.getFullYear() === endDate
						.getFullYear())) {

					var oDateFormatWithFromHour = DateFormat.getInstance({
						pattern: "d. MMM y",
						UTC: true
					});

					var oDateFormatWithHourWithoutDate = DateFormat.getInstance({
						pattern: "HH:mm",
						UTC: true
					});

					var absenceTypeObj = viewModel.getData().absenceTypeSet[absenceType];

					var klokken = this.getModel("i18n").getResourceBundle().getText(
						"HolidayRequest-kl");

					return oDateFormatWithFromHour.format(startDate) + ":\n" + klokken + " " + oDateFormatWithHourWithoutDate.format(startDate) + "-" +
						oDateFormatWithHourWithoutDate.format(endDate);
				} else {
					var oDateFormat = DateFormat.getInstance({
						pattern: "d. MMM y",
						UTC: true
					});
					return oDateFormat.format(startDate) + " - " + oDateFormat.format(endDate);
				}
			}
		},

	});

});